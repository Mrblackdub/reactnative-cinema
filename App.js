import React from 'react';
import { NavigationContainer } from '@react-navigation/native';

import AppNavigator from './src/components/AppNavigator';
import { StatusBar, Text, View, LogBox } from 'react-native';
import { AppLoading } from 'expo';
import { useFonts, NunitoSans_200ExtraLight, NunitoSans_400Regular, NunitoSans_600SemiBold } from "@expo-google-fonts/nunito-sans";

import firebase from 'firebase';

import { firebaseConfig } from './src/config/keys'

import { Provider, useDispatch } from 'react-redux';
import store from './src/hooks/store';
import { SIGNIN_SUCCESS } from './src/hooks/actionTypes';
import Splash from './src/pages/Splash/Splash';
LogBox.ignoreLogs(['Setting a timer for a long period of time'])


const App = () => {

	const [loading, setLoading] = React.useState(false);

	React.useEffect(() => {

		console.log('hello')

		if(!firebase.apps.length){
			firebase.initializeApp(firebaseConfig);
		}
		
	}, [])

	const [fontsLoaded] = useFonts({
		NunitoSans_200ExtraLight,
		NunitoSans_400Regular,
		NunitoSans_600SemiBold
	})



	if (!fontsLoaded) {
		return (<View style={{ backgroundColor: '#070D2D', flex: 1 }}><Text style={{ color: "white" }}>Loading...</Text></View>)
	}

	return (
		<Provider store={store}>
			<NavigationContainer>
				<StatusBar backgroundColor="#070D2D" />
				<AppNavigator />
			</NavigationContainer>
		</Provider>
	);

}

export default App;

