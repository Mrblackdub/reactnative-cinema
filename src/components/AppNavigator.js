import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'
import TabNavigator from './TabNavigator';
import MovieDetails from '../pages/MovieDetails/MovieDetails';
import Register from '../pages/Register/Register';
import SignIn from '../pages/SignIn/SignIn';
import Splash from '../pages/Splash/Splash';

export default function AppNavigator() {
	const Stack = createStackNavigator();

	return (
		<Stack.Navigator
			initialRouteName="Splash"
		>

			<Stack.Screen
				name="Splash"
				component={Splash}
				options={{
					headerShown: false,
					headerTitle: ''
				}}

			/>

			<Stack.Screen
				component={TabNavigator}
				name="Tab Navigator"
				options={{
					headerShown: false,
					headerTitle: ''
				}}
			/>
			<Stack.Screen
				name="Movie Details"
				component={MovieDetails}
				options={{
					headerShown: false,
					headerTitle: ''
				}}
			/>
			<Stack.Screen
				name="Register"
				component={Register}
				options={{
					headerShown: false,
					headerTitle: ''
				}}
			/>

			<Stack.Screen
				name="SignIn"
				component={SignIn}
				options={{
					headerShown: false,
					headerTitle: ''
				}}
			/>
		</Stack.Navigator>
	)
}
