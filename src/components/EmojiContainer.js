import React from 'react'
import { StyleSheet, Text, View,Dimensions, Pressable } from 'react-native'


const {width} = Dimensions.get('screen')
export default function EmojiContainer({genreName, emoji}) {
	return (
		<Pressable 
			onPress={() => console.log(emoji.name)}
		>
		<View style={styles.container}>
			<View style={styles.emojiWrapper}>
				<Text style={styles.emojiIcon}>
					{emoji.emoji}
				</Text>
			</View>
			<View  style={styles.genreWrapper}>
				<Text  style={styles.genreTitle}>
					{genreName}
				</Text>
			</View>
		</View>
		</Pressable>
	)
}

const styles = StyleSheet.create({
	container: {
		flexDirection: 'column',
		height: 90,
		width: 70,
		backgroundColor: '#161A37',
		alignItems: 'center',
		justifyContent: 'center',
		borderRadius: 25
	},
	emojiWrapper: {
		marginBottom: 5
	},
	emojiIcon: {
		fontSize: 24
	},
	genreWrapper: {

	},
	genreTitle: {
		color: 'white',
		fontSize: 12,
		fontFamily: 'NunitoSans_200ExtraLight'
	}

})
