import React from 'react'
import { StyleSheet, Text, View, Image, Pressable } from 'react-native'

export default function MovieContainer({ item, navigation }) {

	const { title, poster_path, id } = item;
	let mytextvar = title;
	let maxlimit = 15;

	return (
		<Pressable
			onPress={() => {
				navigation.navigate('Movie Details', {id});
			}}
		>
			<View style={styles.container}>
				<View style={styles.imageWrapper}>
					<Image key={poster_path} style={styles.movieImage} source={{ uri: `https://image.tmdb.org/t/p/original/${poster_path}` }} alt="movies" />
				</View>

				<View style={styles.textWrapper}>
					<Text style={styles.movieTitle}>{((mytextvar).length >= maxlimit) ? (((mytextvar).substring(0, maxlimit + 2)) + '...') : mytextvar}
					</Text>
				</View>
			</View>
		</Pressable>
	)
}

const styles = StyleSheet.create({
	container: {
		marginRight: 15,
		overflow: 'hidden',
		width: 200,
		height: 300,
		marginBottom: 20
	},
	imageWrapper: {
		flex: 1,
	},
	movieImage: {
		width: 200,
		height: 250,
		borderRadius: 20
	},
	textWrapper: {
		marginTop: 20
	},
	movieTitle: {
		color: "white",
		fontFamily: "NunitoSans_600SemiBold",
		fontSize: 18,
		marginLeft: 10,
	}
})
