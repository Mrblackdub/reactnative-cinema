import React from 'react'
import { StyleSheet, Text, View, Image, Pressable, useWindowDimensions } from 'react-native'

const space = 10;
export default function SearchComponentItem({ item, navigation }) {

	const dimensions = useWindowDimensions();
	const imageWidth = dimensions.width / 2;

	const { title, poster_path, id } = item;

	return (
		<Pressable
			onPress={() => {
				navigation.push('Movie Details', { id });
			}}
			style={{
				width: imageWidth,
			}}
		>
			<View style={{ marginTop: 10, padding: 10 }}>
				<View style={{ display: 'flex', flexDirection: 'column' }}>
					<View style={{ marginRight: 10 }}>
						{
							poster_path ?
								<Image
									source={{ uri: `https://image.tmdb.org/t/p/w500/${poster_path}` }}
									alt="movies"
									style={{
										width: 120,
										height: 150,
										borderRadius: 7
									}} />
								:
								<View
									style={{
										width: 120,
										height: 150,
										backgroundColor: 'rgba(31, 9, 112, 0.296)',
										borderRadius: 5
									}}
								
								/>
						}

					</View>

					<View style={{ display: 'flex', justifyContent: 'space-around', width: 100, }}>
						<Text style={{ color: 'white', fontSize: 12 }}>{title}</Text>
					</View>

				</View>
			</View>
		</Pressable>
	)
}

