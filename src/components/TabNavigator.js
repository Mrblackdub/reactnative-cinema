import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import HomeNavigator from '../pages/Home/HomeNavigator';
import SearchNavigator from '../pages/Search/SearchNavigator';
import ProfileNavigator from '../pages/Profile/ProfileNavigator';

import { Ionicons } from '@expo/vector-icons'; 
import { Feather } from '@expo/vector-icons'; 
import { MaterialIcons } from '@expo/vector-icons'; 
import CustomBottomTabBar from './CustomBottomTaBar';


const ACTIVE_TAB_COLOR = '#546EE5'
const INACTIVE_TAB_COLOR = '#3E4265'


export default function TabNavigator({navigation}) {
	const Tab = createBottomTabNavigator();

	return (
		<Tab.Navigator
		tabBar = {props => <CustomBottomTabBar {...props} />}
			tabBarOptions={{
				activeTintColor: '#fff',
				style: {
					backgroundColor: '#070D2D',
					borderTopWidth:1,
					borderTopColor:'#070D2D',
					paddingTop: 10,
				}
			}}
		>
			<Tab.Screen 
				name="Home" 
				navigation={navigation} 
				component={HomeNavigator} 
				options={{
					tabBarLabel: '',
					tabBarIcon: ({focused}) => (
						<Ionicons name="home-sharp" size={24} color={focused ? ACTIVE_TAB_COLOR : INACTIVE_TAB_COLOR} />
					)
				}}
				/>
			<Tab.Screen 
			name="Search" 
			component={SearchNavigator}
			options={{
				tabBarLabel: '',
				tabBarIcon: ({focused}) => (
					<MaterialIcons name="search" size={24} color={focused ? ACTIVE_TAB_COLOR : INACTIVE_TAB_COLOR} />
				)
			}}
			/>
			<Tab.Screen 
				name="Profile" 
				component={ProfileNavigator}
				options={{
					tabBarLabel: '',
					tabBarIcon: ({focused}) => (
						<Feather name="user" size={24} color={focused ? ACTIVE_TAB_COLOR : INACTIVE_TAB_COLOR} />
					)
				}}
			/>
		</Tab.Navigator>
	)
}

 