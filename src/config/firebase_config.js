import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/storage';

import {firebaseConfig} from './keys';


class Firebase{
	constructor(){
		this.auth = firebase.auth();
		this.db = firebase.firestore();
	}


	async registerUser(email, password, name) {
		await firebase.auth().createUserWithEmailAndPassword(email, password)
		.then(user => {
			if(user){
				firebase.auth().currentUser.updateProfile({displayName: name})
			}
		})
		
		.catch(error => {return error})

		return user;
	}


	async signoutUser(){
		const signout = await firebase.auth().signOut().catch(err => console.log(err));

		console.log('signout')
		return signout;
	}


	async signinUser(email,password){
		const user = await firebase.auth().signInWithEmailAndPassword(email,password).catch(err => console.log(err));

		return user;
	}
}


export default new Firebase();