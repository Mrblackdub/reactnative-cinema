export const START_FETCHING_MOVIES = 'START_FETCHING_MOVIES';
export const FINISH_FETCHING_MOVIES =  'FINISH_FETCHING_MOVIES';

export const START_FETCHING_MOVIE_BY_ID =  'START_FETCHING_MOVIE_BY_ID';
export const FINISH_FETCHING_MOVIE_BY_ID =  'FINISH_FETCHING_MOVIE_BY_ID';

export const START_FETCHING_SIMILAR_MOVIES =  'START_FETCHING_SIMILAR_MOVIES';
export const FINISH_FETCHING_SIMILAR_MOVIES =  'FINISH_FETCHING_SIMILAR_MOVIES';

export const START_FETCHING_TOP_RATED_MOVIES =  'START_FETCHING_TOP_RATED_MOVIES';
export const FINISH_FETCHING_TOP_RATED_MOVIES =  'FINISH_FETCHING_TOP_RATED_MOVIES';

export const START_SEARCHING_MOVIES = 'START_SEARCHING_MOVIES';
export const FINISH_SEARCHING_MOVIES = 'FINISH_SEARCHING_MOVIES';

export const AUTH_SUCCESS = 'AUTH_SUCCESS';
export const AUTH_ERROR = 'AUTH_ERROR'; 

export const REGISTER_REQUEST = 'REGISTER_REQUEST';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_FAIL = 'REGISTER_FAIL' 

export const LOGOUT_USER = 'LOGOUT_USER';

export const SIGNIN_REQUEST = 'SIGNIN_REQUEST';
export const SIGNIN_SUCCESS = 'SIGNIN_SUCCESS';
export const SIGNIN_FAIL = 'SIGNIN_FAIL' 