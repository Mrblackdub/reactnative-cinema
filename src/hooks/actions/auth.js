import { REGISTER_REQUEST, REGISTER_SUCCESS, REGISTER_FAIL, LOGOUT_USER, SIGNIN_REQUEST, SIGNIN_SUCCESS, SIGNIN_FAIL } from '../actionTypes';
import firebase from 'firebase'

const authFailMessages = (errorCode) => {
	switch (errorCode) {
		case 'auth/invalid-email':
			return 'Email is invalid.';
		case 'auth/user-disabled':
			return 'User is disabled.';
		case 'auth/wrong-password':
			return 'Password is invalid.';
		case 'auth/email-already-in-use':
			return 'Email address is already in use.';
		case 'auth/weak-password':
			return 'Password is not strong enough. Password must have more than 6 characters';
		default:
			return 'Authentication failed';
	}

}

export const register = (email, password, name) => async dispatch => {
	dispatch({ type: REGISTER_REQUEST })

	firebase.auth().createUserWithEmailAndPassword(email, password)
		.then(user => {
			if (user.user) {
				user.user.updateProfile({
					displayName: name
				})
				dispatch({
					type: REGISTER_SUCCESS,
					payload: user
				})
			}
		}).catch(error => dispatch({ type: REGISTER_FAIL, payload: authFailMessages(error.code) }))
}

export const signout = () => async dispatch => {
	dispatch({ type: LOGOUT_USER })
	firebase.auth().signOut();
}


export const signin = (email, password) => async dispatch => {
	dispatch({ type: SIGNIN_REQUEST })

	firebase.auth().signInWithEmailAndPassword(email, password)
		.then(user => {
			dispatch({ type: SIGNIN_SUCCESS, payload: user })
		}).catch(error => dispatch({ type: SIGNIN_FAIL, payload: authFailMessages(error.code) }))

}