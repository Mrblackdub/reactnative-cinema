import {
	START_FETCHING_MOVIES, 
	FINISH_FETCHING_MOVIES, 
	START_FETCHING_MOVIE_BY_ID, 
	FINISH_FETCHING_MOVIE_BY_ID,
	START_FETCHING_SIMILAR_MOVIES,
	FINISH_FETCHING_SIMILAR_MOVIES,
	START_SEARCHING_MOVIES,
	FINISH_SEARCHING_MOVIES,
	START_FETCHING_TOP_RATED_MOVIES,
	FINISH_FETCHING_TOP_RATED_MOVIES
} from '../actionTypes';

export const fetchMovies = () => async dispatch => {
	dispatch({type: START_FETCHING_MOVIES});

	const response = await fetch(`https://api.themoviedb.org/3/movie/popular?api_key=9b5af6d9f992e2550944919a011154b7&language=en-US&page=1`);
	const data = await response.json();
	dispatch({
		type: FINISH_FETCHING_MOVIES,
		payload: data.results
	})
}


export const fetchTopRatedMovies = () => async dispatch => {
	dispatch({type: START_FETCHING_TOP_RATED_MOVIES});

	const response = await fetch(`https://api.themoviedb.org/3/movie/top_rated?api_key=9b5af6d9f992e2550944919a011154b7&language=en-US&page=1`);
	const data = await response.json();
	dispatch({
		type: FINISH_FETCHING_TOP_RATED_MOVIES,
		payload: data.results
	})
}



export const fetchMovieById = (movie_id) => async dispatch => {
	dispatch({type: START_FETCHING_MOVIE_BY_ID});

	const response = await fetch(`https://api.themoviedb.org/3/movie/${movie_id}?api_key=9b5af6d9f992e2550944919a011154b7`);
	const data = await response.json();

	dispatch({
		type: FINISH_FETCHING_MOVIE_BY_ID,
		payload: data
	})
}


export const fetchSimilarMovies = (id) => async dispatch => {
	dispatch({type: START_FETCHING_SIMILAR_MOVIES});

	const response = await fetch(`https://api.themoviedb.org/3/movie/${id}/similar?api_key=9b5af6d9f992e2550944919a011154b7&language=en-US&page=1`)
	const data = await response.json();
	
	dispatch({
		type: FINISH_FETCHING_SIMILAR_MOVIES,
		payload: data.results
	})
}

export const searchMovies = (term) => async dispatch => {
	dispatch({type: START_SEARCHING_MOVIES})

	const response = await fetch(`https://api.themoviedb.org/3/search/movie?api_key=9b5af6d9f992e2550944919a011154b7&language=en-US&query=${term}&page=1&include_adult=false`);
	const data = await response.json();

	dispatch({
		type: FINISH_SEARCHING_MOVIES,
		payload: data.results
	})
}