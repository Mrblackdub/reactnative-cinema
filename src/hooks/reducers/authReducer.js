import {
	REGISTER_REQUEST,
	REGISTER_SUCCESS,
	REGISTER_FAIL,
	LOGOUT_USER,
	SIGNIN_SUCCESS,
	SIGNIN_REQUEST,
	SIGNIN_FAIL
} from '../actionTypes';

const initialState = {
	user: null,
	loading: false,
	error: ''
}

export const authReducer = (state = initialState, action) => {
	switch (action.type) {
		case REGISTER_REQUEST:
			return {
				...state,
				loading: true
			}
		case REGISTER_SUCCESS:
			return {
				...state,
				loading: false,
				user: action.payload,
			}

		case REGISTER_FAIL:
			return {
				...state,
				loading: false,
				error: action.payload
			}
		case SIGNIN_REQUEST:
			return {
				...state,
				loading: true
			}
		case SIGNIN_SUCCESS:
			return {
				...state,
				loading: false,
				user: action.payload,
			}

		case SIGNIN_FAIL:
			return {
				...state,
				loading: false,
				error: action.payload
			}

		case LOGOUT_USER:
			return {
				...state,
				...initialState
			}

		default:
			return state;
	}
}

