import {combineReducers} from 'redux';
import {movieReducer} from './movieReducer';
import {movieByIdReducer} from './movieByIdReducer';
import {similarMoviesReducer} from './similarMoviesReducer';
import {searchMoviesReducer} from './searchMoviesReducer';
import {topratedMoviesReducer} from './topratedMoviesReducer';
import {authReducer} from './authReducer';

export default combineReducers({
	movies: movieReducer,
	movieById: movieByIdReducer,
	similarMovies: similarMoviesReducer,
	searchMovies: searchMoviesReducer,
	topratedMovies: topratedMoviesReducer,
	auth: authReducer
})	