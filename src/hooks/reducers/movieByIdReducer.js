import movies from '../../data/movies';
import { START_FETCHING_MOVIE_BY_ID, FINISH_FETCHING_MOVIE_BY_ID } from '../actionTypes';

const initialState = {
	loading: false,
	movies: null,
}

export const movieByIdReducer = (state = initialState, action) => {
	switch (action.type) {
		case START_FETCHING_MOVIE_BY_ID:
			return {
				...state,
				loading: true
			}
		case FINISH_FETCHING_MOVIE_BY_ID:
			return {
				...state,
				loading: false,
				movies: action.payload
			}

		default:
			return state;
	}
}