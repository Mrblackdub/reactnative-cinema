import { START_FETCHING_MOVIES, FINISH_FETCHING_MOVIES } from '../actionTypes';

const initialState = {
	loading: false,
	movies: [],
	text: 'one'
}

export const movieReducer = (state = initialState, action) => {
	switch (action.type) {
		case START_FETCHING_MOVIES:
			return {
				...state,
				loading: true
			}
		case FINISH_FETCHING_MOVIES:
			return {
				...state,
				loading: false,
				movies: action.payload
			}

		default:
			return state;
	}
}