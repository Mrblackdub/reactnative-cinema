import { START_SEARCHING_MOVIES, FINISH_SEARCHING_MOVIES } from '../actionTypes';

const initialState = {
	loading: false,
	movies: [],
}

export const searchMoviesReducer = (state = initialState, action) => {
	switch (action.type) {
		case START_SEARCHING_MOVIES:
			return {
				...state,
				loading: true
			}
		case FINISH_SEARCHING_MOVIES:
			return {
				...state,
				loading: false,
				movies: action.payload
			}

		default:
			return state;
	}
}