import { START_FETCHING_SIMILAR_MOVIES, FINISH_FETCHING_SIMILAR_MOVIES } from '../actionTypes';

const initialState = {
	loading: false,
	similarMovies: [],
	text: 'one'
}

export const similarMoviesReducer = (state = initialState, action) => {
	switch (action.type) {
		case START_FETCHING_SIMILAR_MOVIES:
			return {
				...state,
				loading: true
			}
		case FINISH_FETCHING_SIMILAR_MOVIES:
			return {
				...state,
				loading: false,
				similarMovies: action.payload
			}

		default:
			return state;
	}
}