import { START_FETCHING_TOP_RATED_MOVIES, FINISH_FETCHING_TOP_RATED_MOVIES } from '../actionTypes';

const initialState = {
	loading: false,
	movies: [],
	text: 'one'
}

export const topratedMoviesReducer = (state = initialState, action) => {
	switch (action.type) {
		case START_FETCHING_TOP_RATED_MOVIES:
			return {
				...state,
				loading: true
			}
		case FINISH_FETCHING_TOP_RATED_MOVIES:
			return {
				...state,
				loading: false,
				movies: action.payload
			}

		default:
			return state;
	}
}