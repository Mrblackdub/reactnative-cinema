import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from './HomeScreen';
import { View, Text, StyleSheet, Image, Pressable,ActivityIndicator } from 'react-native';
import avatar from '../../../assets/image.jpeg';
import { useSelector } from 'react-redux';

import firebase from 'firebase';




const customHeader = ({navigation}) => {
	const [name, setName] = React.useState('');

	React.useEffect(() => {
		const user = firebase.auth().currentUser;
		if(user){
			setName(firebase.auth().currentUser.displayName);
		}
	},[])

	return (
		<View style={styles.headerContainer}>
			<View style={styles.userName}>
				<Text style={styles.userNameText}>Hi, {name}  !</Text>
			</View>
			<Pressable onPress={() => navigation.navigate('Register')}>
			<View style={styles.avatarContainer}>
				<Image source={{uri: 'https://static.wikia.nocookie.net/marvelcinematicuniverse/images/b/b0/Spider-Man_FFH_Profile.jpg/revision/latest?cb=20190917181733'}} style={{width: 50, height: 50, borderRadius: 50}} />
			</View>
			</Pressable> 
		</View>
	)
} 

const styles = StyleSheet.create({
	headerContainer: {
		height: 100,
		flexDirection: 'row',
		backgroundColor: '#070D2D',
		alignItems: 'center',
		justifyContent: 'space-between',
		paddingTop: 15,
	},
	userName: {
		color: 'white',
		paddingLeft: 20
	},
	userNameText:{
		color: "white",
		fontFamily: 'NunitoSans_400Regular',
		fontSize: 20,
	},
	avatarContainer:{
		paddingRight: 25
	}
})


const HomeNavigator = ({navigation}) => {
  const Stack = createStackNavigator();

  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
				component={HomeScreen}
				
				options={{
					title: '',
					header: customHeader,
					headerStyle: {elevation: 0}
				}}

      />
    </Stack.Navigator>
  );
};

export default HomeNavigator;