import React from 'react';
import { View, Text, StyleSheet, Dimensions, TextInput, ScrollView, ActivityIndicator } from 'react-native';
import { Octicons } from '@expo/vector-icons';
import { connect, useDispatch, useSelector } from 'react-redux';

import EmojiContainer from '../../components/EmojiContainer';
import MovieContainer from '../../components/MovieContainer'
import { fetchMovies, fetchTopRatedMovies } from '../../hooks/actions/fetchMovies';

const { width } = Dimensions.get('screen')


const emojiCatagories = [
	{
		name: 'Horror',
		emoji: '\u{1F631}'
	},
	{
		name: 'Romance',
		emoji: '\u{1F970}'
	},
	{
		name: 'Comedy',
		emoji: '\u{1F602}'
	},
	{
		name: 'Drama',
		emoji: '\u{1F62E}'
	}
]


const HomeScreen = ({ navigation }) => {
	const [popularMovieList, setPopularMovieList] = React.useState([]);
	const [loading, setLoading] = React.useState(false);

	const [topRatedList, setTopRatedList] = React.useState([]);
	const [topRatedLoading, setTopRatedLoading] = React.useState(false);

	const dispatch = useDispatch();
	const moviesStore = useSelector(state => state)

	React.useEffect(() => {
		dispatch(fetchMovies());
		dispatch(fetchTopRatedMovies())
	}, [])

	React.useEffect(() => {
		setLoading(moviesStore.movies.loading);
		setPopularMovieList(moviesStore.movies.movies);

		setTopRatedLoading(moviesStore.topratedMovies.loading);
		setTopRatedList(moviesStore.topratedMovies.movies)
	})


	return (
		<ScrollView style={styles.container}>

			<View style={styles.categories}>
				<View style={styles.categoriesTitle}>
					<Text style={styles.categoriesText}>Categories</Text>
					<Text style={styles.SeeMoreText}>See more</Text>
				</View>

				<View style={styles.categoriesItems}>
					{
						emojiCatagories.map((item, index) => {
							return (
								<EmojiContainer emoji={item} genreName={item.name} key={index} />
							)
						})
					}
				</View>

			</View>

			<View style={styles.categories}>
				<View style={styles.categoriesTitle}>
					<Text style={styles.categoriesText}>Popular</Text>
				</View>

				<View style={styles.categoriesItems}>
					{
						(loading) ?
							<View style={[styles.container, { display: 'flex', alignItems: 'center', justifyContent: 'center' }]}>
								<ActivityIndicator size="large" color="#fff" />
							</View> :

							<ScrollView
								horizontal={true}
								showsHorizontalScrollIndicator={false}
							>
								{
									popularMovieList.map((item, index) => {
										return (
											<MovieContainer navigation={navigation} item={item} key={index} />
										)
									})
								}
							</ScrollView>
					}

				</View>

			</View>


			<View style={styles.categories}>
				<View style={styles.categoriesTitle}>
					<Text style={styles.categoriesText}>Top Rated</Text>
				</View>

				<View style={styles.categoriesItems}>
					{
						(topRatedLoading) ?
							<View style={[styles.container, { display: 'flex', alignItems: 'center', justifyContent: 'center' }]}>
								<ActivityIndicator size="large" color="#fff" />
							</View> :

							<ScrollView
								horizontal={true}
								showsHorizontalScrollIndicator={false}
							>
								{
									topRatedList.map((item, index) => {
										return (
											<MovieContainer navigation={navigation} item={item} key={index} />
										)
									})
								}
							</ScrollView>
					}

				</View>

			</View>

		</ScrollView>
	)
};


const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#070D2D",
		paddingRight: 20,
		paddingLeft: 20,
	},
	searchSection: {
		flexDirection: 'row',
		alignItems: 'center',
		backgroundColor: 'transparent',
		borderWidth: 1,
		borderColor: '#2B2B3D',
		borderRadius: 40,
		padding: 10
	},
	searchIcon: {
		padding: 10
	},
	textInput: {
		flex: 1,
		color: '#9094AF',
		paddingTop: 10,
		paddingRight: 20,
		paddingBottom: 10,
		paddingLeft: 0,
		fontSize: 15,
		fontWeight: '500',
		fontFamily: "NunitoSans_400Regular",
	},

	categories: {
		marginTop: 25,
		justifyContent: 'center',
	},
	categoriesTitle: {
		paddingTop: 10,
		paddingBottom: 10,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center'
	},
	categoriesText: {
		color: "#F8F8F8",
		fontSize: 20,
		fontFamily: "NunitoSans_600SemiBold"
	},
	SeeMoreText: {
		color: '#9094AF',
		fontSize: 14,
		fontFamily: "NunitoSans_400Regular",
		paddingRight: 5
	},
	categoriesItems: {
		flexDirection: 'row',
		marginTop: 10,
		justifyContent: 'space-between'
	}
})



export default HomeScreen;