import { Ionicons, AntDesign } from '@expo/vector-icons'
import React from 'react'
import { Pressable, StyleSheet, Text, View, Image, ScrollView, Dimensions, Animated, ActivityIndicator } from 'react-native'
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { useDispatch, useSelector } from 'react-redux';
import { fetchMovieById, fetchSimilarMovies } from '../../hooks/actions/fetchMovies';
import SimilarMoviesContainer from '../../components/SimilarMoviesContainer';


const { width, height } = Dimensions.get('screen');
const HEADER_HEIGHT = 100
const insets = 50


export default function MovieDetails({ route, navigation }) {
	const animatedVal = React.useRef(new Animated.Value(0)).current;
	const [item, setItem] = React.useState(null);
	const [loading, setLoading] = React.useState(false);

	const [similarMovies, setSimilarMovies] = React.useState(null);
	const [loadingSimilarMovies, setLoadingSimilarMovies] = React.useState(false);

	const insets = useSafeAreaInsets();

	const dispatch = useDispatch();
	const moviesStore = useSelector(state => state)

	React.useEffect(() => {
		let { id } = route.params;
		dispatch(fetchMovieById(id))
		dispatch(fetchSimilarMovies(id))
	}, [])



	React.useEffect(() => {
		setLoading(moviesStore.movieById.loading)
		setItem(moviesStore.movieById.movies)
		setLoadingSimilarMovies(moviesStore.similarMovies.loading)
		setSimilarMovies(moviesStore.similarMovies.similarMovies)
	})



	const headerHeight = animatedVal.interpolate({
		inputRange: [0, HEADER_HEIGHT],
		outputRange: [HEADER_HEIGHT, HEADER_HEIGHT - 50],
		extrapolate: 'clamp'
	});

	if (loading || item === null) {
		return (
			<View style={[styles.container, { display: 'flex', alignItems: 'center', justifyContent: 'center' }]}>
				<ActivityIndicator size="large" color="#fff" />
			</View>
		)
	} else {
		return (
			<View style={styles.container}>
				<Animated.View style={[styles.header, { height: headerHeight }]}>
					<View style={styles.backArrow}>
						<Pressable
							onPress={() => navigation.goBack()}
						>
							<Ionicons name="chevron-back-sharp" size={26} color={"#F8F8F8"} />
						</Pressable>
					</View>

					<View style={styles.detailTitle}>
						<Text style={styles.detailTitleText}>
							Detail movie
					</Text>
					</View>

					<View style={styles.bookmark}>
						<Pressable
							onPress={() => navigation.goBack()}
						>
							<Ionicons name="bookmark-outline" size={24} color="#F8f8f8" />
						</Pressable>
					</View>
				</Animated.View>

				<ScrollView
					showsVerticalScrollIndicator={false}
					scrollEventThrottle={16}
					onScroll={Animated.event(
						[{ nativeEvent: { contentOffset: { y: animatedVal } } }],
						{ useNativeDriver: false }
					)}

				>
					<View style={styles.mainContainer}>
						<View style={styles.imageContainer}>
							<Image resizeMode="cover" style={{ zIndex: 2, height: 350, width: 275, borderRadius: 30 }} source={{ uri: `https://image.tmdb.org/t/p/original/${item.poster_path}` }} alt="movies" />
							<View
								style={{
									top: 50,
									position: 'absolute',
									height: 350 * 0.9,
									width: 275 * 0.9,
									backgroundColor: '#fff',
									borderRadius: 30,
									opacity: .3,
									zIndex: 1,
									elevation: 0,

								}} />

							<View
								style={{
									top: 75,
									position: 'absolute',
									height: 350 * 0.85,
									width: 275 * 0.85,
									backgroundColor: '#fff',
									borderRadius: 30,
									opacity: .2,
									zIndex: 0,
									elevation: 0,
								}} />
						</View>

						<View style={styles.movieInformation}>
							<Text style={styles.movieTitle}>{item.title}</Text>
						</View>

						<View style={styles.movieInformation}>
							<Text style={styles.runtime}>Runtime: {item.runtime}</Text>
							<View style={{ marginHorizontal: 10 }}><Text style={{ color: "#808292", fontSize: 18 }}>|</Text></View>
							<View style={styles.rating}>
								<Text style={{ color: "#f8f8f8", fontSize: 20, marginRight: 10 }}>{item.vote_average}</Text>
								<AntDesign name="star" size={18} color="#FFA235" />
							</View>
						</View>

						<View style={styles.movieGenre}>
							<ScrollView
								horizontal={true}
								showsHorizontalScrollIndicator={false}
							>
								{item.genres.map((genre, i) => {
									return (
										<View key={i}
											style={{ backgroundColor: '#161A37', borderRadius: 40, paddingVertical: 5, paddingHorizontal: 15, marginRight: 10 }}>
											<Text style={{ fontFamily: 'NunitoSans_200ExtraLight', fontSize: 12, color: "#F8f8f8" }}>{genre.name}</Text>
										</View>
									)
								})}
							</ScrollView>
						</View>

						<View style={styles.synopsisContainer}>
							<View style={styles.synopsisTitle}>
								<Text style={{ color: "#f8f8f8", fontFamily: 'NunitoSans_600SemiBold', fontSize: 18 }}>Synopsis</Text>
							</View>
							<View>
								<Text style={styles.synopsisText}>
									{item.overview}
								</Text>
							</View>
						</View>


						<View style={styles.categories}>
							<View style={styles.categoriesTitle}>
								<Text style={styles.categoriesText}>Similar movies</Text>
							</View>

							<View style={styles.categoriesItems}>
								{
									loadingSimilarMovies ?
										<View style={[styles.container, { display: 'flex', alignItems: 'center', justifyContent: 'center' }]}>
											<ActivityIndicator size="large" color="#fff" />
										</View>
										:
										<ScrollView
											horizontal={true}
											showsHorizontalScrollIndicator={false}
										>
											{
												similarMovies.slice(0, 5).map((item, index) => {
													return (
														<SimilarMoviesContainer key={item.title} navigation={navigation} item={item} key={index} />

													)
												})
											}
										</ScrollView>
								}

							</View>

						</View>

					</View>
				</ScrollView>

			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#070D2D",
		paddingRight: 20,
		paddingLeft: 20,
	},
	header: {
		height: HEADER_HEIGHT,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center'
	},
	backArrow: {
	},

	detailTitle: {

	},
	detailTitleText: {
		color: "#F8F8F8",
		fontSize: 20,
		fontFamily: "NunitoSans_400Regular"
	},

	bookmark: {

	},

	mainContainer: {
		alignContent: 'center'
	},
	imageContainer: {
		alignItems: 'center',
		marginBottom: 20
	},

	movieInformation: {
		marginTop: 20,
		flexDirection: 'row',
		alignItems: 'center',
	},

	movieTitle: {
		color: "#F8F8F8",
		fontFamily: 'NunitoSans_600SemiBold',
		fontSize: 26
	},
	runtime: {
		color: "#F8F8F8",
		fontFamily: 'NunitoSans_400Regular',
		fontSize: 18
	},

	rating: {
		flexDirection: 'row',
		alignItems: 'center',
	},

	movieGenre: {
		flexDirection: 'row',
		marginVertical: 15,
	},

	synopsisContainer: {
		marginVertical: 10
	},

	synopsisTitle: {
		marginBottom: 10
	},
	synopsisText: {
		color: "#747788"
	},
	addToFav: {
		width: 150,
		height: 50,
		position: 'absolute',
		elevation: 0,
		backgroundColor: '#546EE5',
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: 20,
		top: height - 125,
		left: (width / 2) - 75
	},
	categories: {
		marginTop: 25,
		justifyContent: 'center',
	},
	categoriesTitle: {
		paddingTop: 10,
		paddingBottom: 10,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center'
	},
	categoriesText: {
		color: "#F8F8F8",
		fontSize: 20,
		fontFamily: "NunitoSans_600SemiBold"
	},
	categoriesItems: {
		flexDirection: 'row',
		marginTop: 10,
		justifyContent: 'space-between'
	}
})
