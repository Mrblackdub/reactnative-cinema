import { Ionicons, AntDesign } from '@expo/vector-icons'
import React from 'react'
import { StyleSheet, Text, View, Image, Dimensions, TouchableOpacity, ActivityIndicator } from 'react-native'
import { useDispatch, useSelector } from 'react-redux';
import { signout } from '../../hooks/actions/auth';
import firebase from 'firebase';

const { width, height } = Dimensions.get('screen');
const HEADER_HEIGHT = 70


export default function Profile({ navigation }) {
	
	const [name, setName] = React.useState('');

	const dispatch = useDispatch();
	const state = useSelector(state => state);

	React.useEffect(() => {
		const user = firebase.auth().currentUser;
		if(user){
			setName(firebase.auth().currentUser.displayName);
		}
	},[]);

	if (state.auth.loading) {
		return (
			<View style={[styles.container, { display: 'flex', alignItems: 'center', justifyContent: 'center' }]}>
				<ActivityIndicator size="large" color="#fff" />
			</View>
		)
	}
	else{
	return (
		<View style={styles.container}>
			<View style={[styles.header]}>
					<TouchableOpacity
					style={styles.button}
					onPress={
						() => {
							dispatch(signout());
							navigation.navigate('SignIn');
						}
					}
				>
					<Text style={styles.buttonText}>Sign out</Text>
				</TouchableOpacity>
				<View style={styles.detailTitle}>
					<Text style={styles.detailTitleText}>
						Profile
					</Text>
				</View>
			</View>


			<View style={styles.mainContent}>

				<View style={styles.profileInfo}>
					<View style={styles.userAvatar}>
						<Image
							source={{ uri: 'https://static.wikia.nocookie.net/marvelcinematicuniverse/images/b/b0/Spider-Man_FFH_Profile.jpg/revision/latest?cb=20190917181733' }}
							style={{
								width: 150,
								height: 150,
								borderRadius: 100
							}} />
					</View>
					<View style={styles.userName}>
						<Text style={styles.userNameText}>{name}</Text>
					</View>
					<View style={styles.subInfo}>
						<Text style={styles.subInfoText}>Ethiopia</Text>
					</View>
				</View>

				<View style={styles.moviesContainer}>
					<Text style={styles.moviesListText}>Liked Movies</Text>
					<View style={styles.moviesList}>
						{/* MovieContainer */}
					</View>
				</View>

			</View>
		</View>
	)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#070D2D",
		paddingRight: 20,
		paddingLeft: 20,
	},
	header: {
		height: HEADER_HEIGHT,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent:'center',
	},
	detailTitle:{
		display: 'flex',
	},
	detailTitleText: {
		color: "white",
		fontSize: 20,
		fontFamily: "NunitoSans_600SemiBold",
	},
	mainContent: {
		flex: 1
	},
	profileInfo: {
		alignItems: 'center',
		marginTop: 20
	},
	userName: {
		marginTop: 10
	},
	userNameText: {
		color: "white",
		fontFamily: "NunitoSans_400Regular",
		fontSize: 25
	},
	subInfo: {
		marginTop: 1
	},
	subInfoText: {
		color: "white",
		fontFamily: "NunitoSans_200ExtraLight",
	},

	moviesContainer: {
		flex: 1,
		paddingTop: 50
	},
	moviesListText: {
		color: "white",
		fontFamily: "NunitoSans_400Regular",
		fontSize: 20
	},

	button: {
		alignItems: 'center',
		backgroundColor: '#184157',
		width: 75,
		height: 44,
		borderRadius: 10,
		display: 'flex',
		justifyContent: 'center',
		position: 'absolute',
		top: 15,
		left: 0
	},
	buttonText: {
		color: 'white',
	},
})
