import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Profile from './Profile';

const CheckoutNavigator = () => {
  const Stack = createStackNavigator();

  return (
    <Stack.Navigator
			screenOptions={{
				headerShown: false
			}}
		
		>
      <Stack.Screen
        name="Profile Screen"
				component={Profile}
      />
    </Stack.Navigator>
  );
};

export default CheckoutNavigator;