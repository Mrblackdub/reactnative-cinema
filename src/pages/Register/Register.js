import { Ionicons, AntDesign } from '@expo/vector-icons'
import React from 'react'
import { StyleSheet, Text, View, Dimensions, TextInput, TouchableOpacity, ActivityIndicator } from 'react-native'
import { useDispatch, useSelector } from 'react-redux';
import { register } from '../../hooks/actions/auth';

import firebase from 'firebase';

const { width, height } = Dimensions.get('screen');
const HEADER_HEIGHT = 70


export default function Register({ navigation }) {
	const [name, setName] = React.useState('');
	const [email, setEmail] = React.useState('');
	const [password, setPassword] = React.useState('');

	const dispatch = useDispatch();
	const state = useSelector(state => state);


	const registerNewUser = async () => {
		dispatch(register(email, password, name));

		firebase.auth().onAuthStateChanged((user) => {
			if (user) {
				navigation.push("Tab Navigator", { screen: 'Home' });
			}
		})


		setName('');
		setEmail('');
		setPassword('');
	}

	const renderButton = () => {
		if (state.auth.loading) {
			return <ActivityIndicator size="large" color="#fff" />
		}

		return (
			<TouchableOpacity  
				style={styles.button}
				onPress={() => registerNewUser(email, password, name)}
			>
				<Text style={styles.buttonText}>Sign Up</Text>
			</TouchableOpacity>
		)
	}


	return (
		<View style={styles.container}>
			<View style={[styles.header]}>
				<View style={styles.detailTitle}>
					<Text style={styles.detailTitleText}>
						Register
					</Text>
				</View>
			</View>
			<View style={styles.error}>

				{
					state.auth.error ?
						<Text style={styles.errorText}>
							{state.auth.error}
						</Text>
						:
						<View />
				}

			</View>

			<View style={styles.formContainer}>
				<View style={styles.searchSection}>
					<Ionicons style={styles.searchIcon} size={20} name="person" color="#fff" />
					<TextInput
						placeholder="name"
						placeholderTextColor="#9094AF"
						value={name}
						onChangeText={(name) => setName(name)}
						style={styles.textInput}
					/>
				</View>
				<View style={styles.searchSection}>
					<Ionicons style={styles.searchIcon} size={20} name="ios-mail-sharp" color="#fff" />
					<TextInput
						placeholder="email-address"
						placeholderTextColor="#9094AF"
						value={email}
						onChangeText={(email) => setEmail(email)}
						style={styles.textInput}
						keyboardType='email-address'
					/>
				</View>
				<View style={styles.searchSection}>
					<Ionicons style={styles.searchIcon} size={20} name="lock-closed" color="#fff" />
					<TextInput
						placeholder="password"
						placeholderTextColor="#9094AF"
						value={password}
						onChangeText={(password) => setPassword(password)}
						style={styles.textInput}
						secureTextEntry={true}
					/>
				</View>

				{renderButton()}


				<TouchableOpacity
					style={styles.link}
					onPress={() => navigation.navigate('SignIn')}
				>
					<Text style={styles.linkText}>Registered? Sign In</Text>
				</TouchableOpacity>
			</View>
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#070D2D",
		paddingRight: 20,
		paddingLeft: 20,
		justifyContent: 'center',
	},
	header: {
		height: HEADER_HEIGHT,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center'
	},
	detailTitleText: {
		color: "white",
		fontSize: 20,
		fontFamily: "NunitoSans_600SemiBold"
	},
	formContainer: {
		alignItems: 'center'
	},
	searchSection: {
		flexDirection: 'row',
		alignItems: 'center',
		backgroundColor: 'transparent',
		borderWidth: 1,
		borderColor: '#2B2B3D',
		borderRadius: 40,
		padding: 10,
		margin: 10
	},
	searchIcon: {
		padding: 10
	},
	textInput: {
		flex: 1,
		color: '#9094AF',
		paddingTop: 10,
		paddingRight: 20,
		paddingBottom: 10,
		paddingLeft: 0,
		fontSize: 15,
		fontWeight: '500',
		fontFamily: "NunitoSans_400Regular",
	},

	button: {
		alignItems: 'center',
		backgroundColor: '#184157',
		width: 150,
		height: 44,
		padding: 10,
		borderRadius: 25,
		marginTop: 10,
		marginBottom: 20
	},

	buttonText: {
		color: 'white',
	},

	link: {
		marginTop: 10
	},

	linkText: {
		color: '#fff',
		fontSize: 14,
		fontFamily: "NunitoSans_400Regular",
	},
	error: {
		textAlign: 'center',
		alignItems: 'center',
	},
	errorText: {
		color: 'tomato',
		fontSize: 15
	}
})
