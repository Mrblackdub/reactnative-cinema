import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import SearchScreen from './SearchScreen';

const HomeNavigator = () => {
	const Stack = createStackNavigator();

	return (
		<Stack.Navigator
			screenOptions={{
				headerShown: false
			}}
		>
			<Stack.Screen
				name="Search Screen"
				component={SearchScreen}
			/>
		</Stack.Navigator>
	);
};

export default HomeNavigator;