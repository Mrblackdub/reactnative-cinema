import { Ionicons, AntDesign } from '@expo/vector-icons'
import React from 'react'
import { Pressable, StyleSheet, Text, View, Image, ScrollView, Dimensions, Animated, TextInput, ActivityIndicator, FlatList } from 'react-native'
import { useDispatch, useSelector } from 'react-redux';
import SearchComponentItem from '../../components/SearchComponentItem';

import { searchMovies } from '../../hooks/actions/fetchMovies'

const { width, height } = Dimensions.get('screen');
const HEADER_HEIGHT = 100


export default function SearchScreen({ navigation }) {
	const [text, setText] = React.useState('');
	const [movies, setMovies] = React.useState([]);
	const [loading, setLoading] = React.useState(false)

	const dispatch = useDispatch();
	const movieStore = useSelector(state => state)




	React.useEffect(() => {
		if(text !== ''){
			dispatch(searchMovies(text))
			setLoading(movieStore.searchMovies.loading)
			setMovies(movieStore.searchMovies.movies)
		}

		if(text === ''){
			setLoading(false)
			setMovies([])
		}

	}, [text])


	return (
		<View style={styles.container}>
			<View style={[styles.header]}>
				<View style={styles.backArrow}>
					<Pressable
						onPress={() => navigation.goBack()}
					>
						<Ionicons name="chevron-back-sharp" size={26} color={"#F8F8F8"} />
					</Pressable>
				</View>

				<View style={styles.detailTitle}>
					<Text style={styles.detailTitleText}>
						Search Movies
					</Text>
				</View>
			</View>

			<View style={styles.searchSection}>
				<Ionicons style={styles.searchIcon} size={20} name="search" color="#fff" />
				<TextInput
					placeholder="Search Movies"
					placeholderTextColor="#9094AF"
					value={text}
					onChangeText={(text) => setText(text)}
					style={styles.textInput}
				/>
			</View>



			<View style={styles.maincontent}>
				{
					(loading) ?
						<View style={[styles.container, { display: 'flex', alignItems: 'center', justifyContent: 'center', marginTop: 20 }]}>
							<ActivityIndicator size="large" color="#fff" />
						</View> :
						<View>
							<FlatList
								data={movies}
								renderItem={(movie) => <SearchComponentItem index={movie.index} navigation={navigation} item={movie.item} />}
								keyExtractor={(item, index) => index.toString()}
								numColumns={2}
								ListFooterComponent={<View />}
								ListFooterComponentStyle={{height: 200}}
							/>

						</View>


				}


			</View>
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#070D2D",
		paddingRight: 20,
		paddingLeft: 20,
	},
	header: {
		height: HEADER_HEIGHT,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center'
	},
	detailTitleText: {
		color: "#F8F8F8",
		fontSize: 20,
		fontFamily: "NunitoSans_600SemiBold"
	},

	backArrow: {
		marginRight: 'auto'
	},
	searchSection: {
		flexDirection: 'row',
		alignItems: 'center',
		backgroundColor: 'transparent',
		borderWidth: 1,
		borderColor: '#2B2B3D',
		borderRadius: 40,
		padding: 10
	},
	searchIcon: {
		padding: 10
	},
	textInput: {
		flex: 1,
		color: '#9094AF',
		paddingTop: 10,
		paddingRight: 20,
		paddingBottom: 10,
		paddingLeft: 0,
		fontSize: 15,
		fontWeight: '500',
		fontFamily: "NunitoSans_400Regular",
	},

	mainContent:{
		marginTop: 100
	}

})
