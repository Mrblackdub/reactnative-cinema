import React from 'react'
import { StyleSheet, Text, View, SafeAreaView, ActivityIndicator } from 'react-native';

import firebase from 'firebase';
import { useSelector } from 'react-redux';

export default function Splash({ navigation }) {
	const [animating, setAnimation] = React.useState(true);

	const state = useSelector(state => state)


	React.useEffect(() => {
		console.log('firebase', firebase.auth().currentUser)
		console.log(state.auth.user)
	if(firebase.auth().currentUser !== null){
			navigation.push("Tab Navigator", { screen: 'Home' });
		}else{
				navigation.navigate('Register');
		}
	}, [])


	return (
		<SafeAreaView style={{ flex: 1, backgroundColor: '#070D2D' }}>
			<View style={{
				alignContent: 'center',
				justifyContent: 'center',
				flex: 1
			}}>
				<ActivityIndicator
					animating={animating}
					size='large'
					color='white'
				/>
			</View>
		</SafeAreaView>
	)
}
