import { Ionicons, AntDesign } from '@expo/vector-icons'
import React from 'react'
import { Pressable, StyleSheet, Text, View, Image, ScrollView, Dimensions, Animated, TextInput, TouchableOpacity, Alert } from 'react-native'

export default function Welcome({ navigation }){

	return (
		<View style={styles.container}>
			<Text>What should i Watch?</Text>

			<TouchableOpacity onPress={() => navigation.navigate('Register') }>
				<Text style={styles.text}>Go to register</Text>
			</TouchableOpacity>
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#070D2D",
		paddingRight: 20,
		paddingLeft: 20,
		justifyContent: 'center',
	},
	text:{
		color: '#fff',
		fontSize: 20,
		fontFamily: "NunitoSans_600SemiBold",
		textTransform: 'uppercase'
	}
})
